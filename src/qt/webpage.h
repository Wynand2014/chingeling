#ifndef WEBPAGE_H
#define WEBPAGE_H

#include <QWidget>
#include <QNetworkAccessManager>

namespace Ui {
class WebPage;
}

class WebPage : public QWidget
{
    Q_OBJECT

public:
    explicit WebPage(QWidget *parent = 0);
    ~WebPage();

private:
    Ui::WebPage *ui;
    QNetworkAccessManager netman;

private slots:
    void handleChingelingUri(QNetworkReply *reply);
};

#endif // WEBPAGE_H
